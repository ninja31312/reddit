# README #

It's a Reddit-like app.

### Features ###

- Users can the browse topics.
- Users can post a new topic by tapping the cell "Share your topics".
- Users can upvote and downvote the topics in a descending order of upvoteCount.

### Solved problems ###

* Thread-safe issue
The topics are stored in a mutableArray and there are many threads try to mutate the mutableArray, so we should apply a mechanism to prevent accessing the mutableArray from the crash. The serial queue is the savior because it is "First-in, First-out" for every mutating operation. Therefore, we can ensure the mutableArray is thread-safe.
In the reading operation case, we always apply an immutableArray by shallow-copy the mutableArray because we can not expose the mutable property in the header for other objects to mutate it.

* Sort the topics in a descending order of upvoteCount

    Assumption: The topics are empty at the first time.
    
    We sort the topics in a bubble way.

    e.g. (direction of array: left to right, top to bottom)

    Create the topics, topics: [A,B,C], upvoteCount: [0,0,0]

    After a user click B, topics: [B,A,C], upvoteCount: [1,0,0]

    After a user click A, topics: [B,A,C], upvoteCount: [1,1,0]

    After a user click A, topics: [A,B,C], upvoteCount: [2,1,0]

    After a user click C, topics: [A,B,C], upvoteCount: [2,1,1]

    After a user click C, topics: [A,C,B], upvoteCount: [2,2,1]

    After a user click C, topics: [C,A,B], upvoteCount: [3,2,1]


### Components ###

- NJTopicListViewController is responsible for browsing the topics.
- NJTopicEditingViewController is responsible for adding/editing the topics.
- NJRedditAPIManager is responsible for accessing the API.
- NJMockServerStoreManager is responsible for applying mock data.
- NJUserManager is responsible for applying mock user.
- NJTopic is the topic model.
- NJUser is the user model.