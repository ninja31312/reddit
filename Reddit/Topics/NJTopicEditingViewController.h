//
//  NJTopicEditingViewController.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NJTopic.h"

@class NJTopicEditingViewController;

@protocol NJTopicEditingViewControllerDelegate <NSObject>

- (void)topicEditingViewController:(NJTopicEditingViewController *)viewController didFinishEditingTopicWithNewTopicArray:(NSArray <NJTopic *>*)newTopicArray;

@end
@interface NJTopicEditingViewController : UIViewController

- (instancetype)initWithTopicString:(NSString *)topicString;
@property (nonatomic, weak) id <NJTopicEditingViewControllerDelegate> delegate;

@end
