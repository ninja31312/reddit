//
//  NJTopicTextViewCell.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "AppUtilities.h"
#import "NSLayoutConstraint+VisualFormat.h"
#import "NJTopicTextViewCell.h"

static CGFloat const kMinTextViewCellHeight = 600.0f; // For clicking easier
static NSUInteger const kMaxWordCount = 255;
static CGFloat const kTextFontSize = 17.0f;

@interface NJTextView: UITextView

@end

@implementation NJTextView

// For fixing the jumpping problem for the content when typing
- (void)setContentOffset:(CGPoint)contentOffset
{
    if (self.frame.size.height >= self.contentSize.height) {
        [super setContentOffset:CGPointZero];
        return;
    }
    [super setContentOffset:contentOffset];

}
@end

@interface NJTopicTextViewCell () <UITextViewDelegate>

@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, assign) BOOL everResetPlaceholder;

@end

@implementation NJTopicTextViewCell

+ (CGFloat)cellHeightWithText:(NSString *)text width:(CGFloat)width
{
    if (!text) {
        return kMinTextViewCellHeight;
    }
    CGSize size = [text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:kTextFontSize]} context:nil].size;

    return MAX(ceilf(size.height), kMinTextViewCellHeight);
}

+ (NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.textView.text = @"";
    self.delegate = nil;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NJTextView *textView = [[NJTextView alloc] init];
        self.textView = textView;
        [self.contentView addSubview:textView];
        textView.delegate = self;
        textView.returnKeyType = UIReturnKeyDone;
        // For calculating correct height
        textView.textContainerInset = UIEdgeInsetsZero;
        textView.font = [UIFont systemFontOfSize:kTextFontSize];
        [NSLayoutConstraint activateConstraintsWithFormat:@"V:|-0-[v0]-0-|" views:@[textView]];
        [NSLayoutConstraint activateConstraintsWithFormat:@"H:|-0-[v0]-0-|" views:@[textView]];
        textView.textColor = [UIColor lightGrayColor];
        textView.text = NJLocalizedString(@"Create a topic...");
    }
    return self;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (!self.everResetPlaceholder) {
        self.textView.text = @"";
        self.textView.textColor = [UIColor blackColor];
        self.everResetPlaceholder = YES;
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    NSUInteger textLength = textView.text.length;
    if (textLength >= kMaxWordCount) {
        textView.text = [textView.text substringWithRange:NSMakeRange(0, kMaxWordCount)];
        textLength = textView.text.length;
    }
    if ([self.delegate respondsToSelector:@selector(NJTopicTextViewCell:didChangeToText:)]) {
        [self.delegate NJTopicTextViewCell:self didChangeToText:[textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    NSUInteger textLength = textView.text.length + text.length;
    if (textLength > kMaxWordCount) {
        return NO;
    }
    return YES;
}

@end
