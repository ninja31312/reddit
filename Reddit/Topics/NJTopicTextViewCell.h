//
//  NJTopicTextViewCell.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NJTopicTextViewCell;

@protocol NJTopicTextViewCellDelegate <NSObject>

@optional
- (void)NJTopicTextViewCell:(NJTopicTextViewCell *)cell didChangeToText:(NSString *)text;

@end


@interface NJTopicTextViewCell : UICollectionViewCell

+ (CGFloat)cellHeightWithText:(NSString *)text width:(CGFloat)width;
+ (NSString *)cellIdentifier;

@property (nonatomic, weak) id <NJTopicTextViewCellDelegate> delegate;

@end
