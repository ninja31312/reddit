//
//  NJVoteView.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUtilities.h"

@class NJVoteView;

@protocol NJVoteViewDelegate <NSObject>

- (void)voteView:(NJVoteView *)voteView didSelectType:(NJVoteType)voteType;

@end

@interface NJVoteView : UIView

+ (CGFloat)viewHeight;
- (instancetype)initWithVoteType:(NJVoteType)voteType;
- (instancetype)init NS_UNAVAILABLE;
- (void)updateVoteCount:(NSUInteger)voteCount;

@property (nonatomic, weak) id <NJVoteViewDelegate> delegate;

@end
