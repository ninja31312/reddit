//
//  NJTopicEditingViewController.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NJUserManager.h"
#import "NSLayoutConstraint+VisualFormat.h"

#import "NJTopicEditingViewController.h"
#import "NJUserMessageCell.h"
#import "NJTopicTextViewCell.h"
#import "NJRedditAPIManager.h"

typedef enum : NSUInteger {
    EditTopicSectionUserMessage,
    EditTopicSectionText,
    EditTopicSectionCount
} EditTopicSectionType;

@interface NJTopicEditingViewController () <UICollectionViewDataSource, UICollectionViewDelegate, NJTopicTextViewCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSString *currentEditingTopicString;

@end

@implementation NJTopicEditingViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithTopicString:(NSString *)topicString
{
    self = [super init];
    if (self) {
        self.currentEditingTopicString = topicString;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    self.title = self.currentEditingTopicString ?  NJLocalizedString(@"Edit topic") : NJLocalizedString(@"New topic");
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NJLocalizedString(@"Post") style:UIBarButtonItemStylePlain target:self action:@selector(post:)];
    self.navigationItem.rightBarButtonItem.enabled = self.currentEditingTopicString;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NJLocalizedString(@"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)];

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView = collectionView;
    [self.view addSubview:collectionView];
    collectionView.backgroundColor = [UIColor whiteColor];
    collectionView.alwaysBounceVertical = YES;

    [NSLayoutConstraint activateConstraintsWithFormat:@"V:|-0-[v0]-0-|" views:@[collectionView]];
    [NSLayoutConstraint activateConstraintsWithFormat:@"H:|-0-[v0]-0-|" views:@[collectionView]];
    collectionView.dataSource = self;
    collectionView.delegate = self;

    [collectionView registerClass:[NJUserMessageCell class] forCellWithReuseIdentifier:[NJUserMessageCell cellIdentifier]];
    [collectionView registerClass:[NJTopicTextViewCell class] forCellWithReuseIdentifier:[NJTopicTextViewCell cellIdentifier]];

    // Keyboard event handling
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)post:(UIBarButtonItem *)sender
{
    NJUser *user = [[NJUserManager sharedManager] currentUser];
    NJTopic *topic = [NJTopic topicWithContent:self.currentEditingTopicString postingUserIdentifier:user.identifier];
    [[NJRedditAPIManager sharedManager] postTopic:topic success:^(NSArray <NJTopic *> *topicArray) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
            [self.delegate topicEditingViewController:self didFinishEditingTopicWithNewTopicArray:topicArray];
        }];
    } failure:^(NSError *error) {
        //TODO: Error handling
    }];
}

- (void)cancel:(UIBarButtonItem *)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return EditTopicSectionCount;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case EditTopicSectionUserMessage:
            return 1;
        case EditTopicSectionText:
            return 1;
        default:
            return 0;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case EditTopicSectionUserMessage:
        {
            NJUserMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NJUserMessageCell cellIdentifier] forIndexPath:indexPath];
            NJUser *user = [[NJUserManager sharedManager] currentUser];
            [cell setImageKey:user.imageKey];
            [cell setMessage:user.name];
            return cell;
        }
        case EditTopicSectionText:
        {
            NJTopicTextViewCell *textViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:[NJTopicTextViewCell cellIdentifier] forIndexPath:indexPath];
            textViewCell.delegate = self;
            return textViewCell;
        }
        default:
            return nil;
    }
}

#pragma mark - UICollectionViewDataDelegate


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat collectionViewWidth = CGRectGetWidth(self.collectionView.frame);
    switch (indexPath.section) {
        case EditTopicSectionUserMessage:
            return CGSizeMake(collectionViewWidth, [NJUserMessageCell cellHeight]);
        case EditTopicSectionText:
            //TODO: Apply real data
            return CGSizeMake(collectionViewWidth, [NJTopicTextViewCell cellHeightWithText:self.currentEditingTopicString width:collectionViewWidth]);
        default:
            return CGSizeZero;
    }
}

#pragma mark - NJTopicTextViewCellDelegate

- (void)NJTopicTextViewCell:(NJTopicTextViewCell *)cell didChangeToText:(NSString *)text
{
    self.currentEditingTopicString = text;
    self.navigationItem.rightBarButtonItem.enabled = (text.length > 0);
    [self.collectionView.collectionViewLayout invalidateLayout];
}

#pragma mark - Keyboard notification handling

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSValue *value = userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [value CGRectValue];
    CGFloat keyboardHeight = CGRectGetHeight(keyboardFrame) - self.bottomLayoutGuide.length;
    UIEdgeInsets temp = self.collectionView.contentInset;
    temp.bottom = keyboardHeight;
    self.collectionView.contentInset = temp;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    UIEdgeInsets temp = self.collectionView.contentInset;
    temp.bottom = self.bottomLayoutGuide.length;
    self.collectionView.contentInset = temp;
}

@end
