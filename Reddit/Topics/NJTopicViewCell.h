//
//  NJTopicViewCell.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUtilities.h"

#import "NJTopic.h"

@class NJTopicViewCell;

@protocol NJTopicViewCellDelegate <NSObject>

- (void)topicViewCell:(NJTopicViewCell *)cell didSelectTopic:(NJTopic *)topic forVoteType:(NJVoteType)voteType;

@end

@interface NJTopicViewCell : UICollectionViewCell

+ (CGFloat)cellHeightWithTopic:(NSString *)topicString width:(CGFloat)width;
+ (NSString *)cellIdentifier;
//TODO: the edge case: exceed max value of vouteCount
- (void)updateVoteCount:(NSUInteger)voteCount forType:(NJVoteType)voteType;

@property (nonatomic, strong) NJTopic *topic;
@property (nonatomic, weak) id <NJTopicViewCellDelegate> delegate;

@end
