//
//  NJVoteView.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NSLayoutConstraint+VisualFormat.h"
#import "AppUtilities.h"
#import "NJVoteView.h"

static CGFloat const kVoteImageViewLength = 64.0f;
static CGFloat const kCountLabelHeight = 20.0f;

@interface NJVoteView ()

@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, strong) UIImageView *voteImageView;
@property (nonatomic, assign) NJVoteType voteType;

@end

@implementation NJVoteView

+ (CGFloat)viewHeight
{
    return kVoteImageViewLength + kCountLabelHeight;
}

- (instancetype)initWithVoteType:(NJVoteType)voteType
{
    self = [super init];
    if (self) {
        self.voteType = voteType;
        
        UILabel *countLabel = [[UILabel alloc] init];
        self.countLabel = countLabel;
        [self addSubview:countLabel];
        countLabel.textAlignment = NSTextAlignmentCenter;
        
        [NSLayoutConstraint activateConstraintsWithFormat:@"H:|-0-[v0]-0-|" views:@[countLabel]];
        countLabel.text = NJLocalizedString(@"0");

        UIImageView *voteImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        [self addSubview:voteImageView];

        voteImageView.image = [UIImage imageNamed:(self.voteType == NJVoteUp) ? @"upvote" : @"downvote"];

        [voteImageView.centerXAnchor constraintEqualToAnchor:voteImageView.superview.centerXAnchor].active = YES;
        [NSLayoutConstraint activateConstraintsWithFormat:[NSString stringWithFormat:@"H:[v0(%f)]", kVoteImageViewLength] views:@[voteImageView]];

        [NSLayoutConstraint activateConstraintsWithFormat:[NSString stringWithFormat:@"V:|-0-[v0(%f)]-0-[v1(%f)]-0-|", kCountLabelHeight, kVoteImageViewLength] views:@[countLabel, voteImageView]];

        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [self addGestureRecognizer:tapGestureRecognizer];
    }
    return self;
}

- (void)updateVoteCount:(NSUInteger)voteCount
{
    self.countLabel.text = [NSString stringWithFormat:@"%lu", voteCount];
}

- (void)handleTap:(UITapGestureRecognizer *)sender
{
    [self.delegate voteView:self didSelectType:self.voteType];
}

@end
