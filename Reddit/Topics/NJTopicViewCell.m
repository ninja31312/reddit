//
//  NJTopicViewCell.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NSLayoutConstraint+VisualFormat.h"
#import "NJTopicViewCell.h"
#import "NJVoteView.h"

static CGFloat const kTextFontSize = 17.0f;
// Because of the different inset between the textView in NJTopicTextViewCell and textLabel in NJTopicViewCell, we apply this inset to fix the issue.
static CGFloat const kTextLabelHorizontalInset = 5.0f;

@interface NJTopicViewCell () <NJVoteViewDelegate>

@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) NJVoteView *upvoteView;
@property (nonatomic, strong) NJVoteView *downvoteView;

@end

@implementation NJTopicViewCell

+ (CGFloat)cellHeightWithTopic:(NSString *)topicString width:(CGFloat)width
{
    if (!topicString || topicString.length == 0) {
        return [NJVoteView viewHeight];
    }
    CGSize size = [topicString boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:kTextFontSize]} context:nil].size;

    return ceilf(size.height) + [NJVoteView viewHeight];
}

+ (NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *textLabel = [[UILabel alloc] init];
        self.textLabel = textLabel;
        [self.contentView addSubview:textLabel];
        textLabel.numberOfLines = 0;
        textLabel.font = [UIFont systemFontOfSize:kTextFontSize];

        [NSLayoutConstraint activateConstraintsWithFormat:[NSString stringWithFormat:@"H:|-%f-[v0]-%f-|", kTextLabelHorizontalInset, kTextLabelHorizontalInset] views:@[textLabel]];

        NJVoteView *upvoteView = [[NJVoteView alloc] initWithVoteType:NJVoteUp];
        self.upvoteView = upvoteView;
        [self.contentView addSubview:upvoteView];
        upvoteView.delegate = self;

        NJVoteView *downvoteView = [[NJVoteView alloc] initWithVoteType:NJVoteDown];
        self.downvoteView = downvoteView;
        [self.contentView addSubview:downvoteView];
        downvoteView.delegate = self;

        [NSLayoutConstraint activateConstraintsWithFormat:@"H:|-0-[v0(==v1)]-0-[v1(==v0)]-0-|" views:@[upvoteView, downvoteView]];

        [NSLayoutConstraint activateConstraintsWithFormat:[NSString stringWithFormat:@"V:|-0-[v0]-0-[v1(%f)]-0-|", [NJVoteView viewHeight]] views:@[textLabel, downvoteView]];

        [NSLayoutConstraint activateConstraintsWithFormat:[NSString stringWithFormat:@"V:[v0]-0-[v1(%f)]-0-|", [NJVoteView viewHeight]] views:@[textLabel, upvoteView]];
        
    }
    return self;
}

- (void)setTopic:(NJTopic *)topic
{
    _topic = topic;
    [self updateVoteCount:topic.upvoteCount forType:NJVoteUp];
    [self updateVoteCount:topic.downvoteCount forType:NJVoteDown];
    self.textLabel.text = topic.content;
}

- (void)updateVoteCount:(NSUInteger)voteCount forType:(NJVoteType)voteType
{
    if (voteType == NJVoteUp) {
        [self.upvoteView updateVoteCount:voteCount];
    } else if (voteType == NJVoteDown) {
        [self.downvoteView updateVoteCount:voteCount];
    }
}

#pragma mark - NJVoteViewDelegate

- (void)voteView:(NJVoteView *)voteView didSelectType:(NJVoteType)voteType
{
    [self.delegate topicViewCell:self didSelectTopic:self.topic forVoteType:voteType];
}

@end
