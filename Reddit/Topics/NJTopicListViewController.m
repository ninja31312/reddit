//
//  NJTopicListViewController.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 6/29/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NSLayoutConstraint+VisualFormat.h"
#import "NJUserManager.h"

#import "NJTopicListViewController.h"
#import "NJTopicEditingViewController.h"
#import "NJUserMessageCell.h"
#import "NJMockServerStoreManager.h"
#import "NJRedditAPIManager.h"
#import "NJTopicViewCell.h"

typedef enum : NSUInteger {
    TopicSectionUserMessage,
    TopicSectionTopics,
    TopicSectionCount
} TopicSectionType;

@interface NJTopicListViewController () <UICollectionViewDataSource, UICollectionViewDelegate, NJTopicEditingViewControllerDelegate, NJTopicViewCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray <NJTopic *> *topicArray;

@end

@implementation NJTopicListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NJLocalizedString(@"Topics");
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.collectionView = collectionView;
    [self.view addSubview:collectionView];
    collectionView.backgroundColor = [UIColor whiteColor];
    collectionView.alwaysBounceVertical = YES;
    
    [NSLayoutConstraint activateConstraintsWithFormat:@"V:|-0-[v0]-0-|" views:@[collectionView]];
    [NSLayoutConstraint activateConstraintsWithFormat:@"H:|-0-[v0]-0-|" views:@[collectionView]];
    collectionView.dataSource = self;
    collectionView.delegate = self;

    [collectionView registerClass:[NJUserMessageCell class] forCellWithReuseIdentifier:[NJUserMessageCell cellIdentifier]];

    [collectionView registerClass:[NJTopicViewCell class] forCellWithReuseIdentifier:[NJTopicViewCell cellIdentifier]];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return TopicSectionCount;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case TopicSectionUserMessage:
            return 1;
        case TopicSectionTopics:
            return self.topicArray.count;
        default:
            return 0;
    }
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case TopicSectionUserMessage:
        {
            NJUserMessageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NJUserMessageCell cellIdentifier] forIndexPath:indexPath];
            NJUser *user = [[NJUserManager sharedManager] currentUser];
            [cell setImageKey:user.imageKey];
            [cell setMessage:NJLocalizedString(@"Share your topics")];
            return cell;
        }
        case TopicSectionTopics:
        {
            NJTopicViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NJTopicViewCell cellIdentifier] forIndexPath:indexPath];
            cell.delegate = self;
            NJTopic *topic = self.topicArray[indexPath.item];
            [cell setTopic:topic];
            return cell;
        }
        default:
            return nil;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case TopicSectionUserMessage:
        {
            NJTopicEditingViewController *topicEditingViewController = [[NJTopicEditingViewController alloc] init];
            topicEditingViewController.delegate = self;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:topicEditingViewController];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
            break;

        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat collectionViewWidth = CGRectGetWidth(self.collectionView.frame);
    switch (indexPath.section) {
        case TopicSectionUserMessage:
            return CGSizeMake(collectionViewWidth, [NJUserMessageCell cellHeight]);
        case TopicSectionTopics:
        {
            NJTopic *topic = self.topicArray[indexPath.item];
            return CGSizeMake(collectionViewWidth, [NJTopicViewCell cellHeightWithTopic:topic.content width:collectionViewWidth]);
        }
        default:
            return CGSizeZero;
    }
}

#pragma mark - NJTopicEditingViewControllerDelegate

- (void)topicEditingViewController:(NJTopicEditingViewController *)viewController didFinishEditingTopicWithNewTopicArray:(NSArray <NJTopic *>*)newTopicArray
{
    self.topicArray = newTopicArray;
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:TopicSectionTopics]];
}

#pragma mark - NJTopicViewCellDelegate

- (void)topicViewCell:(NJTopicViewCell *)cell didSelectTopic:(NJTopic *)topic forVoteType:(NJVoteType)voteType
{
    [[NJRedditAPIManager sharedManager] postVote:voteType forTopic:topic success:^(NSUInteger toIndex, NSArray<NJTopic *> *newTopicArray) {
        // Prevent updating reused cells.
        if ([cell.topic.topicIdentifier isEqualToString:topic.topicIdentifier]) {
            NSUInteger voteCount = (voteType == NJVoteUp) ? topic.upvoteCount : topic.downvoteCount;
            [cell updateVoteCount:voteCount forType:voteType];

            NSIndexPath *fromIndexPath = [self.collectionView indexPathForCell:cell];
            NSIndexPath *toIndexPath = [NSIndexPath indexPathForItem:toIndex inSection:TopicSectionTopics];

            self.topicArray = newTopicArray;
            [self.collectionView moveItemAtIndexPath:fromIndexPath toIndexPath:toIndexPath];
        } else {
            self.topicArray = newTopicArray;
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:TopicSectionTopics]];
        }

    } failure:^(NSError *error) {
        //TODO: error handling
    }];
}

@end
