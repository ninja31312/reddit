//
//  NJTopicPostingCell.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 6/29/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "AppUtilities.h"
#import "NSLayoutConstraint+VisualFormat.h"
#import "NJUserMessageCell.h"

static CGFloat const kContentHeight = 60.0f;
static CGFloat const kInset = 10.0f;

@interface NJUserMessageCell ()

@property (nonatomic, strong) UIImageView *userImageView;
@property (nonatomic, strong) UILabel *messageLabel;

@end

@implementation NJUserMessageCell

+ (CGFloat)cellHeight
{
    return kContentHeight + 2 * kInset;
}

+ (NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.userImageView.image = [UIImage imageNamed:@"userDefault"];
    self.messageLabel.text = NJLocalizedString(@" ");
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *userImageView = [[UIImageView alloc] init];
        self.userImageView = userImageView;
        [self.contentView addSubview:userImageView];
        userImageView.image = [UIImage imageNamed:@"userDefault"];

        UILabel *messageLabel = [[UILabel alloc] init];
        self.messageLabel = messageLabel;
        [self.contentView addSubview:messageLabel];
        messageLabel.text = NJLocalizedString(@" ");

        [NSLayoutConstraint activateConstraintsWithFormat:[NSString stringWithFormat:@"H:|-%f-[v0(%f)]-%f-[v1]-%f-|", kInset, kContentHeight, kInset, kInset] views:@[userImageView, messageLabel]];

        [NSLayoutConstraint activateConstraintsWithFormat:[NSString stringWithFormat:@"V:|-%f-[v0(%f)]", kInset, kContentHeight] views:@[userImageView]];

        [NSLayoutConstraint activateConstraintsWithFormat:[NSString stringWithFormat:@"V:|-%f-[v0(%f)]", kInset, kContentHeight] views:@[messageLabel]];
    }
    return self;
}

- (void)setMessage:(NSString *)message
{
    self.messageLabel.text = message;
}

- (void)setImageKey:(NSString *)imageKey
{
    self.userImageView.image = [UIImage imageNamed:imageKey];
}

@end
