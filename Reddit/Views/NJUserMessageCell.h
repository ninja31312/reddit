//
//  NJUserMessageCell.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 6/29/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NJUserMessageCell : UICollectionViewCell

+ (CGFloat)cellHeight;
+ (NSString *)cellIdentifier;
- (void)setMessage:(NSString *)message;
- (void)setImageKey:(NSString *)imageKey; // Currently, imageKey is imageName

@end
