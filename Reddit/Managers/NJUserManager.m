//
//  NJUserManager.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NJUserManager.h"

@interface NJUserManager ()

@property(nonatomic, strong) NJUser *currentUser;

@end

@implementation NJUserManager

+ (instancetype)sharedManager
{
    static NJUserManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NJUserManager alloc] initPrivate];
    });
    return instance;
}

- (instancetype)initPrivate
{
    self = [super init];
    if (self) {
        self.currentUser = [[NJUser alloc] init];
    }
    return self;
}

@end
