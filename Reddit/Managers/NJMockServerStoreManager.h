//
//  NJMockServerStoreManager.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

/*
 NJMockServerStoreManager is responsible for maintaining the mock data in memory.
 */

#import <Foundation/Foundation.h>
#import "AppUtilities.h"
#import "NJTopic.h"
#import "NJUser.h"

@interface NJMockServerStoreManager : NSObject

+ (instancetype)sharedManager;
- (instancetype)init NS_UNAVAILABLE;

@end

@interface NJMockServerStoreManager (Topic)

/*
 @brief Post a topic
 @param topic The topic for posting
 @param success The callback upon success. Gets a new topicArray.
 @param failure The callback upon failure. Gets a NSError.
 */
- (void)postTopic:(NJTopic *)topic success:(void(^)(NSArray <NJTopic *> *newTopicArray))success failure:(void(^)(NSError *error))failure;

/*
 @brief Post a vote for topic
 @param voteType The type of votinng. Up or Down.
 @param topic The topic for voting
 @param success The callback upon success. 
    toIndex means the new position of the topic.
    newTopicArray means the updated topic array
 @param failure The callback upon failure. Gets a NSError.
 */
- (void)postVote:(NJVoteType)voteType forTopic:(NJTopic *)topic success:(void(^)(NSUInteger toIndex, NSArray <NJTopic *> *newTopicArray))success failure:(void(^)(NSError *error))failure;

- (NSArray <NJTopic *>*)topicArray;

@end
