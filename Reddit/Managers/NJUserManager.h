//
//  NJUserManager.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NJUser.h"

@interface NJUserManager : NSObject

+ (instancetype)sharedManager;
- (instancetype)init NS_UNAVAILABLE;

@property(nonatomic, strong, readonly) NJUser *currentUser;

@end
