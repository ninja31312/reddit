//
//  NJMockServerStoreManager.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NJMockServerStoreManager.h"

static NSUInteger const kMaxTopicCount = 20;

@interface NJMockServerStoreManager ()

@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (nonatomic, strong) NSMutableArray *mutableTopicArray;

@end

@implementation NJMockServerStoreManager

+ (instancetype)sharedManager
{
    static NJMockServerStoreManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NJMockServerStoreManager alloc] initPrivate];
    });
    return instance;
}

- (instancetype)initPrivate
{
    self = [super init];
    if (self) {
        // background serial queue
        NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
        operationQueue.maxConcurrentOperationCount = 1;
        self.operationQueue = operationQueue;
        self.mutableTopicArray = [NSMutableArray array];
    }
    return self;
}

- (void)postTopic:(NJTopic *)topic success:(void(^)(NSArray <NJTopic *> *topicArray))success failure:(void(^)(NSError *error))failure
{
    [self.operationQueue addOperationWithBlock:^{
        [self.mutableTopicArray addObject:topic];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                success(self.topicArray);
            }
        });
    }];
}

- (void)postVote:(NJVoteType)voteType forTopic:(NJTopic *)topic success:(void(^)(NSUInteger toIndex, NSArray <NJTopic *> *newTopicArray))success failure:(void(^)(NSError *error))failure
{
    [self.operationQueue addOperationWithBlock:^{
        __block NSUInteger fromIndex = 0;
        [self.topicArray enumerateObjectsUsingBlock:^(NJTopic * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([item.topicIdentifier isEqualToString:topic.topicIdentifier]) {
                if (voteType == NJVoteUp) {
                    [item increaseUpvoteCount];
                } else if (voteType == NJVoteDown) {
                    [item increaseDownvoteCount];
                }
                fromIndex = idx;
                *stop = YES;
            }
        }];
        NSUInteger toIndex = [self moveUpTopicFromIndex:fromIndex];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success) {
                success(toIndex, self.topicArray);
            }
        });
    }];
}

- (NSUInteger)moveUpTopicFromIndex:(NSUInteger)fromIndex
{
    if (fromIndex == 0) {
        return fromIndex;
    }
    NSArray *topicArray = self.topicArray;
    NJTopic *currentTopic = topicArray[fromIndex];
    NSUInteger toIndex = fromIndex;
    for (NSInteger i = fromIndex - 1; i >= 0; i--) {
        NJTopic *topic = topicArray[i];
        if (currentTopic.upvoteCount > topic.upvoteCount) {
            toIndex = i;
        } else {
            break;
        }
    }
    if (fromIndex == toIndex) {
        return fromIndex;
    }
    // Move the topicArray for sorting.
    NSMutableArray *mutableTopicArray = [topicArray mutableCopy];
    [mutableTopicArray removeObjectAtIndex:fromIndex];
    [mutableTopicArray insertObject:currentTopic atIndex:toIndex];
    self.mutableTopicArray = mutableTopicArray;
    return toIndex;
}

- (NSArray <NJTopic *>*)topicArray
{
    NSArray *topicArray = [self.mutableTopicArray copy];
    if (topicArray.count < kMaxTopicCount) {
        return topicArray;
    }
    return [topicArray subarrayWithRange:NSMakeRange(0, kMaxTopicCount)];
}

@end
