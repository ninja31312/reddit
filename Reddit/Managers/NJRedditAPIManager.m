//
//  NJRedditAPIManager.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NJRedditAPIManager.h"
#import "NJMockServerStoreManager.h"

@implementation NJRedditAPIManager

+ (instancetype)sharedManager
{
    static NJRedditAPIManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NJRedditAPIManager alloc] initPrivate];
    });
    return instance;
}

- (instancetype)initPrivate
{
    self = [super init];
    if (self) {
    }
    return self;
}

@end

@implementation NJRedditAPIManager (Topic)


- (void)postTopic:(NJTopic *)topic success:(void(^)(NSArray <NJTopic *> *newTopicArray))success failure:(void(^)(NSError *error))failure
{
    [[NJMockServerStoreManager sharedManager] postTopic:topic success:success failure:failure];
}

- (void)postVote:(NJVoteType)voteType forTopic:(NJTopic *)topic success:(void(^)(NSUInteger toIndex, NSArray <NJTopic *> *newTopicArray))success failure:(void(^)(NSError *error))failure
{
    [[NJMockServerStoreManager sharedManager] postVote:voteType forTopic:topic success:success failure:failure];
}

@end
