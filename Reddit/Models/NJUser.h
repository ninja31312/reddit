//
//  NJUser.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <Foundation/Foundation.h>

//TODO: It's a fake user.
@interface NJUser : NSObject

@property (nonatomic, strong, readonly) NSString *name;
@property (nonatomic, strong, readonly) NSString *imageKey; // Currently, it's a image name.
@property (nonatomic, strong, readonly) NSString *identifier;
@end
