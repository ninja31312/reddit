//
//  NJTopic.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NJTopic : NSObject

// static factory method
+ (instancetype)topicWithContent:(NSString *)content postingUserIdentifier:(NSString *)postingUserIdentifier;
@property (nonatomic, copy, readonly) NSString *content;
@property (nonatomic, copy, readonly) NSString *postingUserIdentifier;
@property (nonatomic, copy, readonly) NSString *topicIdentifier;
@property (nonatomic, assign, readonly) NSUInteger upvoteCount;
@property (nonatomic, assign, readonly) NSUInteger downvoteCount;

- (void)increaseUpvoteCount;
- (void)increaseDownvoteCount;

@end
