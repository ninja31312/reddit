//
//  NJTopic.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NJTopic.h"
#import "AppUtilities.h"

@interface NJTopic ()

@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *postingUserIdentifier;
@property (nonatomic, copy) NSString *topicIdentifier;
@property (nonatomic, assign) NSUInteger upvoteCount;
@property (nonatomic, assign) NSUInteger downvoteCount;

@end

@implementation NJTopic

+ (instancetype)topicWithContent:(NSString *)content postingUserIdentifier:(NSString *)postingUserIdentifier
{
    NJTopic *topic = [[NJTopic alloc] init];
    topic.content = content;
    topic.postingUserIdentifier = postingUserIdentifier;
    // generate the topic identifier automatically.
    topic.topicIdentifier = UUID();
    return topic;
}

- (void)increaseUpvoteCount
{
    self.upvoteCount += 1;
}

- (void)increaseDownvoteCount
{
    self.downvoteCount += 1;
}

@end
