//
//  NJUser.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 7/2/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NJUser.h"

@interface NJUser ()

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageKey;
@property (nonatomic, strong) NSString *identifier;

@end

@implementation NJUser

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.imageKey = @"userDefault";
        self.name = @"Joe";
        self.identifier = @"123e4567-e89b-12d3-a456-426655440000";
    }
    return self;
}
@end
