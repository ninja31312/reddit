//
//  main.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 6/29/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
