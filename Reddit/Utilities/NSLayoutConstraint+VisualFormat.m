//
//  NSLayoutConstraint+VisualFormat.m
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 6/29/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import "NSLayoutConstraint+VisualFormat.h"

@implementation NSLayoutConstraint (VisualFormat)

+ (void)activateConstraintsWithFormat:(NSString *)format views:(NSArray *)views
{
    [NSLayoutConstraint activateConstraintsWithFormat:format views:views options:NSLayoutFormatDirectionLeadingToTrailing];
}

+ (void)activateConstraintsWithFormat:(NSString *)format views:(NSArray *)views options:(NSLayoutFormatOptions)options
{
    // map each view to the corresponding index sequentially
    NSMutableDictionary *mutableViewsDicitionary = [NSMutableDictionary dictionaryWithCapacity:views.count];
    for (NSUInteger i = 0 ; i < views.count; i++) {
        NSString *key = [NSString stringWithFormat:@"v%lu", i];
        UIView *view = views[i];
        view.translatesAutoresizingMaskIntoConstraints = NO;
        mutableViewsDicitionary[key] = view;
    }
    NSDictionary *viewsDictionary = [mutableViewsDicitionary copy];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:format options:options metrics:nil views:viewsDictionary]];
}

@end
