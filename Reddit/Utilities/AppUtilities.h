//
//  AppUtilities.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 6/29/17.
//  Copyright © 2017 NJ. All rights reserved.
//

@import Foundation;
#define NJLocalizedString(key) (NSLocalizedString(key, key))

typedef enum : NSUInteger {
    NJVoteUp,
    NJVoteDown
} NJVoteType;

// Generate uniqe identifier for topic identifier.
static NSString *UUID()
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}
