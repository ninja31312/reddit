//
//  NSLayoutConstraint+VisualFormat.h
//  Reddit
//
//  Created by  Ching-Fan Hsieh on 6/29/17.
//  Copyright © 2017 NJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (VisualFormat)

+ (void)activateConstraintsWithFormat:(NSString *)format views:(NSArray *)views;
+ (void)activateConstraintsWithFormat:(NSString *)format views:(NSArray *)views options:(NSLayoutFormatOptions)options;

@end
